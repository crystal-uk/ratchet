<?php
namespace crystal\ratchet;

use craft\elements\Entry;
use craft\errors\AssetTransformException;

use yii\web\HttpException;


class Ratchet {

  public $config;
  public $arrays;

  function __construct($config = []) {
    $this->arrays = [
      'fields' => [],
      'types' => [],
      'rename' => [],
      'only' => [],
      'include' => [],
      '^include' => [],
      'exclude' => [],
      '^exclude' => [],
      'transforms' => [],
    ];

    $this->config = array_merge([
      'root' => 'array',
      'children' => false,
      'url' => false, // url false?
      'debug' => false,
    ], $this->arrays, $config);
  }

  public static $fieldTransformerMap = [
    'craft\fields\Tags' => 'array',
    'craft\fields\Table' => 'table',
    'craft\fields\Color' => 'colour',
    'craft\fields\Assets'  => 'array',
    'craft\fields\Entries' => 'array',
    'craft\fields\Matrix'  => 'array',
    'craft\fields\Categories' => 'array',
    'craft\fields\Dropdown' => 'selected',
    'craft\fields\RadioButtons' => 'selected',
    'supercool\buttonbox\fields\Buttons' => 'selected',
    'supercool\buttonbox\fields\Colours' => 'selected',
    'verbb\supertable\fields\SuperTableField' => 'array',
  ];

  // run function for backwards compatibility
  public function run($input, $depth = 0) {
    return $this->elements($input, $depth);
  }

  public function elements($input, $depth = 0) {
    if (is_array($input)) {
      if ($this->config['root'] === 'object' ) {
        return self::object($input, $depth);
      }
      return self::array($input, $depth);
    } else {
      return self::single($input, $depth);
    }
  }

  public function fields($element, $fields, $depth = 0) {
    if ($single = !is_array($fields)) {
      $fields = [$fields];
    }

    $fields = array_reduce($element->fieldLayout->fields, function ($carry, $field) use ($element, $fields, $depth) {
      if (in_array($field->handle, $fields)) {
        $carry[$field->handle] = $this->singleCustom($element, $field, $depth);
      }

      return $carry;
    }, []);


    // return $fields;
    return $single ? end($fields): $fields;
  }

  // handle single element
  private function single($element, $depth = 0, $field = null) {
    if (!$element) return ""; 
    
    $conf = $this->flattenConfig($element->type->handle ?? '', $depth, $field);

    return array_merge(
      $this->buildStandard($element, $depth, $conf), 
      $this->buildCustom($element, $depth, $conf), 
      $this->buildInclude($element, $depth, $conf['include']), 
      $this->buildChildren($element->children, $depth, $conf['children'])
    );
  }

  // handle array of elements
  private function array($elements = null, $depth, $field = null) {
    // if not eager loaded, run query
    if (!is_array($elements)) {
      $elements = $elements->all();
    }
    
    $limit = $field->limit ?? false;
    $static = $field->staticField ?? false;
    
    // if field has limit of one element return object instead of array
    // if ($field && ($limit === '1' || $static === '1')) {
    //   if (!$elements) {
    //     return null;
    //   }
    //   return self::single($elements[0], $depth, $field);
    // }

    // run single for each element in array
    return array_map(function($element) use ($depth, $field) {
      return self::single($element, $depth, $field);
    }, $elements);
  }

  private function object($elements = null, $depth) {
    return array_reduce($elements, function ($carry, $element) use ($depth) {
      $carry[$element->slug] = self::single($element, $depth);

      return $carry;
    }, []);
  }

  private function flattenConfig($type, $depth, $field) {
    // overwrite global config with element type config
    $conf = array_merge(
      $this->config, 
      $this->config['types'][$type] ?? []
    );
    
    // overwrite element type with field type config
    if ($field && $field['handle']) {
      $conf = array_merge($conf, $this->config['fields'][$field['handle']] ?? []);
    }
    
    foreach (array_keys($this->arrays) as $option) {
      if (!is_array($conf[$option])) {
        throw new HttpException(500, "Ratchet: '$option' must be array, depth: $depth");
      }
    }

    // merge in ^ top level include/exclude
    if ($depth === 0) {
      $conf['include'] = array_merge($conf['include'], $conf['^include']);
      $conf['exclude'] = array_merge($conf['exclude'], $conf['^exclude']);
    }

    return $conf;
  }

  private function buildStandard($element, $depth, $conf) {
    $standard = [
      'id' => $element->id,
    ];

    if ($element->title) {
      $standard['title'] = $element->title;
    }

    if ($element->slug) {
      $standard['slug'] = $element->slug;
    }
    
    if ($element->url) {
      $standard['url'] = $element->url;
    }

    if ($element->uri) {
      $standard['uri'] = $element->uri;
    }

    if (isset($element->type->handle)) {
      $standard['type'] = $element->type->handle;
    }

    if (isset($element->mimeType)) {
      $standard['mimeType'] = $element->mimeType;
    }

    if (isset($element->filename)) {
      $standard['filename'] = $element->filename;
    }

    if (isset($element->kind) && $element->kind === 'image') {
      $standard = array_merge($standard, $this->buildTransforms($element, $conf['transforms']));
    }

    return $standard;
  }

  private function buildTransforms($element, $conf) {
    if (is_array($conf)) {
      return array_reduce($conf, function ($carry, $transform) use ($element) {
        $carry[$transform] = self::tryTransform($element, $transform);

        return $carry;
      }, []);
    } else if (is_string($conf)) {
      return [$conf => self::tryTransform($element, $conf)];
    }
  }

  private static function tryTransform($element, $transform) {
    try {
      return $element->getUrl($transform);
    } catch (AssetTransformException $exception) {
      return "";
    }
  }

  private function buildInclude($element, $depth, $conf) {
    array_walk($conf, function(&$value, $field) use ($element, $depth) {
      if (is_callable($value)) {
        $value = $value($element, $this);
      }
    });

    return $conf;
  }
  
  private function buildCustom($element, $depth, $conf) {
    // for every field in element field layout
    return array_reduce(
      $element->fieldLayout->fields, 
      function($carry, $field) use ($element, $depth, $conf) {
        
        // if in only list or no only list
        $only = count($conf['only']) ? in_array($field->handle, $conf['only']) : true;
        
        // ignore field if excluded, overwritten by include or rename
        $ignore = 
        !in_array($field->handle, $conf['exclude']) &&
        !in_array($field->handle, $conf['include']) && 
        !in_array($field->handle, array_values($conf['rename']));
        
        if ($ignore && $only) {

          // get possible rename handle 
          $handle = $conf['rename'][$field->handle] ?? $field->handle;

          // create key run function 
          $carry[$handle] = $this->singleCustom($element, $field, $depth);
          
          // show optional debug info
          if ($this->config['debug']) {
            $carry[$field->handle . '_debug'] = [
              'type' => get_class($field),
              'settings' => $field,
              't' => self::$fieldTransformerMap[get_class($field)] ?? 'none',
            ];
            // $carry[$field->handle . '_value'] = $element->fieldValues[$field->handle];
          }
        }
        
        return $carry;
      }, 
      []
    );
  }
  
  private function singleCustom($element, $field, $depth = 0) {
    $value = $element->fieldValues[$field->handle];

    // if transfomer is required get transformer
    $transformer = self::$fieldTransformerMap[get_class($field)] ?? false;

    // recurse down if required
    return $transformer ? self::$transformer($value, $depth + 1, $field) : $value;
  }
  
  private function buildChildren($children, $depth, $conf) {
    if (!count($children) || !$conf) {
      return [];
    }

    if (is_array($conf)) {
      if (!isset($conf[$depth])) {
        return [];
      }
    
      $conf = $conf[$depth];
    }

    $label = $conf === true ? 'children' : $conf;

    return [$label => $this->array($children, $depth + 1)];
  }

  private function selected($data) {
    return [
      'title' => $data->label,
      'slug' => $data->value,
    ];
  }

  private function colour($colour) {
    return $colour->hex;
  }

  private function table($value, $depth, $field) {    
    // return simple array if table only has 1 column
    if (count($columns = $field->columns) === 1) {
      return array_map(function($row) use ($columns) {
        return $row[array_key_first($columns)];
      }, $value ?? []);
    }

    // remove 'col0' keys
    $keys = array_keys($field->columns);
    $value = array_map(function ($row) use ($keys, $field) {
      foreach ($keys as $col) {
        unset($row[$col]);
      }

      return $row;
    }, $value ?? []);

    // flatten array if maxRows is 1
    return $field->maxRows === '1' ? ($value[0] ?? null): $value;
  }
}
