<?php
namespace crystal\ratchet;

use Craft;
use craft\helpers\UrlHelper;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


class Meta {

  public $config;
  public $guzzle;
  public $base;

  public $blackMap = [
    'base' => [
      'link' => ['home', 'author'], 
      'tag' => [], 
      'json' => ['mainEntityOfPage', 'breadcrumbList']
    ],
    'page' => [
      'link' => ['home', 'author'],
      'tag' => [
        'generator', 'referrer', 
        'fb:profile_id', 'fb:app_id', 'og:site_name', 'og:locale', 'og:see_also', 
        'google-site-verification', 'bing-site-verification', 'pinterest-site-verification',
        'twitter:creator', 'twitter:site',
      ],
      'json' => ['creator', 'identity']
    ],
  ];

  function __construct($config = []) {
    $this->guzzle = new Client();

    $this->config = array_merge([
      'cms' => UrlHelper::baseCpUrl(),
      'action' => '/actions/seomatic/meta-container/all-meta-containers/',
      'siteId' => Craft::$app->getSites()->currentSite->id ?? Craft::$app->getSites()->primarySite->id ?? 1,
    ], $config);
  }

  public function base($slim = false) {
    return $this->restructure($this->get(), $slim ? 'base': null);
  }

  public function page($uri, $slim = false) {
    return $this->restructure($this->get($uri), $slim ? 'page': null);
  }

  public function get($uri = '/') {
    $conf = $this->config;

    try {
      $res = $this->guzzle->get($conf['cms'] . $conf['action'], ['query' => [
        'uri' => "/$uri",
        'siteId' => $conf['siteId'],
        'asArray' => true
      ]]);
    } catch (RequestException $error) {
      return $error->getMessage();
    }

    return json_decode($res->getBody(), true);
  }

  // Restructure to match vue-meta
  public function restructure($meta, $type) {
    extract($meta);

    $black = $this->blackMap[$type] ?? [];

    return [
      'title' => $MetaTitleContainer['title']['title'],

      'link' => $this->convert($MetaLinkContainer, $black['link'] ?? []),
      'meta' => $this->convert($MetaTagContainer, $black['tag'] ?? []),

      'script' => $this->microformats($MetaJsonLdContainer, $black['json'] ?? []),

      '__dangerouslyDisableSanitizers' => ['script'],
    ];
  }

  public function convert($container, $black = []) {
    // array_filter will exclude empty values
    $container = array_filter($container);

    // remove blacklisted keys
    foreach ($black as $key => $value) {
      unset($container[$value]);
    }

    // hid allows vue-meta to correctly overwrite values
    return array_reduce(array_keys($container), function ($carry, $key) use ($container) {
      $item = $container[$key];

      if ($item[0] ?? false) {
        // if key has multiple values 
        $carry = array_merge($carry, array_map(function ($child, $i) use ($key) {
          $child['hid'] = $key.'_'.$i;
          return $child;
        }, $item, array_keys($item)));

      } else {
        $item['hid'] = $key;
        $carry[] = $item;
      }

      return $carry;
    }, []);
  }

  public function microformats($container, $black = []) {
    // array_filter will exclude empty values
    $container = array_filter($container);

    foreach ($black as $key => $value) {
      unset($container[$value]);
    }

    return array_values(
      array_map(function ($key, $item) {
        return [
          'type' => 'application/ld+json',
          'innerHTML' => json_encode($item),
          'hid' => $key,
        ];
      }, array_keys($container), $container)
    );
  }

}
